if [ "$#" -eq 0 ]
  then
    echo "Please input version..."
    echo "build.sh <version>"
    exit
fi

echo "Starting..."

echo "npm build..."
rm -rf dist
npm install && npm run build

echo "Building & publishing docker image..."
docker build . -t hub.bmes.vn/dtg-web:"$1" && docker push hub.bmes.vn/dtg-web:"$1"

echo "Success!"
