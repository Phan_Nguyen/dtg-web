import { Component } from '@angular/core';
import {Router} from "@angular/router";

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.css']
})
export class HeaderComponent {

  public menuList = [
    {url: '', name: 'Trang chủ', childrens: [], queryParams: {}},
    {url: 'news/detail/102', name: 'Giới thiệu', childrens: [], queryParams: {}},
    {url: 'news/news-member', name: 'Thành viên', childrens: [], queryParams: {}},
    {url: 'news/news-project', name: 'Dự án', childrens: [], queryParams: {}},
    { name: 'Tin tức', childrens: [
        {
          url: 'news/news-technology',
          name: 'Tin công nghệ',
          childrens: {}, queryParams: {code: 'news-technology'}
        },
        {
          url: 'news/news-market',
          name: 'Tin thị trường',
          childrens: {}, queryParams: {code: 'news-market'}
        },
        {
          url: 'news/news-internal',
          name: 'Tin nội bộ',
          childrens: {}, queryParams: {code: 'news-internal'}
        },
      ], queryParams: {}},
    {url: 'contact', name: 'Liên hệ', childrens: [], queryParams: {}},
  ]

  constructor(private router: Router) {}

  click(url: string, code?: string) {
    this.router.navigate([url, code]);
  }

  hideNavbar() {
    const s = document.querySelector('#navbarNavDropdown');
    const temp = s? s.classList.contains('show'): null;
    if (temp) {
      s? s.classList.remove('show'): '';
    }
    window.scrollTo({
      top: 0,
      behavior: 'smooth',
    });
  }

}
