import {AfterViewInit, Component, OnInit} from '@angular/core';
import {ActivatedRoute, NavigationEnd, PRIMARY_OUTLET, Router} from "@angular/router";
import {filter} from "rxjs";

interface IBreadcrumb {
  label: string;
  url: string;
}
@Component({
  selector: 'app-breadcrumb',
  templateUrl: './breadcrumb.component.html',
  styleUrl: './breadcrumb.component.css'
})
export class BreadcrumbComponent implements OnInit, AfterViewInit {
public breadcrumbs: IBreadcrumb[];
public isQuery = false;

  constructor(private activatedRoute: ActivatedRoute, private router: Router) {
    this.breadcrumbs = [];
  }

  ngOnInit() {
    this.router.events.pipe(filter((event) => event instanceof NavigationEnd)).subscribe(() => {
      const root: ActivatedRoute = this.activatedRoute.root;
      this.breadcrumbs = this.getBreadcrumbs(root);
    });

    const activatedRoot: ActivatedRoute = this.activatedRoute.root;
    this.breadcrumbs = this.getBreadcrumbs(activatedRoot);
  }

  ngAfterViewInit() {}

private getBreadcrumbs(
    route: ActivatedRoute,
    url: string = '',
    breadcrumbs: IBreadcrumb[] = [],
): any {
    const ROUTE_DATA_BREADCRUMB = 'breadcrumb';
    const children: ActivatedRoute[] = route.children;

    if (children.length === 0) {
      return breadcrumbs;
    }

    for (const child of children) {
      if (child.outlet !== PRIMARY_OUTLET) {
        continue;
      }
      let labelTemp = '';
      let code = '';
      let id = '';

      child.paramMap.subscribe((q: any) => {
        code = q.get('code');
        id = q.get('id');

        labelTemp = (code && id) ? id : (code && !id) ? code : id;

        if(labelTemp) {
          this.isQuery = true;
        } else {
          this.isQuery = false;
        }
      });


      if (!child.snapshot.data.hasOwnProperty(ROUTE_DATA_BREADCRUMB) && !this.isQuery) {
        return this.getBreadcrumbs(child, url, breadcrumbs);
      }

      const routeURL: string = child.snapshot.url.map((segment) => segment.path).join('/');

      url += `/${routeURL}`;

      let breadcrumb: IBreadcrumb;
      if (this.isQuery) {
        breadcrumb = {
          label: this.setCode(labelTemp),
          url: url,
        };
      } else {
        breadcrumb = {
          label: child.snapshot.data[ROUTE_DATA_BREADCRUMB],
          url: url,
        };
      }

      breadcrumbs.push(breadcrumb);

      return this.getBreadcrumbs(child, url, breadcrumbs);
    }
  }

  setCode(s: string) : any {
    switch (s) {
      case 'news-technology':
        return 'Tin công nghệ';
      case 'news-market':
        return 'Tin thị trường';
      case 'news-internal':
        return 'Tin nội bộ';
      case 'news-project':
        return 'Dự án';
      case '102':
        return 'Giới thiệu';
      case '103':
        return 'Liên hệ';
      case 'news-member':
        return 'Thành viên';
      default:
        return 'Chi tiết';
    }
  }
}
