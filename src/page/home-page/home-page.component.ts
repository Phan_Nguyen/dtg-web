import {Component, HostListener, OnInit} from '@angular/core';
import {URL_CONFIG} from "../../_config/url";
import {FormControl, FormGroup} from "@angular/forms";
import AOS from "aos";
import {RestApiService} from "../../_services/rest-api.service";
import {Router} from "@angular/router";
import {query} from "@angular/animations";

@Component({
  selector: 'app-home-page',
  templateUrl: './home-page.component.html',
  styleUrls: ['./home-page.component.css']
})
export class HomePageComponent implements OnInit{

  public newsURL = URL_CONFIG.news;

  public form = new FormGroup({
    searchText: new FormControl('')
  });

  public activeTech = true;
  public activeInt = false;
  public activeMar = false;

  public projectTitle: any;
  public projectList: any[] = [];

  public newsDataList: any[] = [] ;
  public partnerList: any[] = [] ;

  ngOnInit() {
    // init aos
    AOS.init();
    AOS.refresh();
    //

    this.loadDataNews();
    this.onSearchNews('news-technology');
    this.getPartners('news-customer');
  }

  constructor(
    private restApiService: RestApiService,
    private router: Router
  ) {}

  loadDataNews() {
    this.projectList = [];
    this.restApiService.multipleRequestInSameTime(
      this.restApiService.getList(this.newsURL,'news-project', 4,1),
    ).subscribe(([projectList]) => {
      this.projectTitle = projectList[0];
      projectList.forEach((p: any, index: number) => {
        if (index > 0 && index < 4) {
          this.projectList.push(p);
        }
      });
    })
  }

  onSearchNews(str : string) {
    if (str === 'news-technology') {
      this.activeTech = true;
      this.activeMar = this.activeInt = false;
      this.getNews('news-technology');
    }
    if (str === 'news-internal') {
      this.activeInt = true;
      this.activeTech = this.activeMar = false;
      this.getNews('news-internal');
    }
    if (str === 'news-market') {
      this.activeMar = true;
      this.activeTech = this.activeInt = false;
      this.getNews('news-market');
    }
  }

  getNews(typecode: string) {
    this.newsDataList = [];
    this.restApiService.getList(this.newsURL,typecode, 3, 1).subscribe((dataList: any) => {
      dataList.forEach((p: any, index: number) => {
        if (index < 4) {
          this.newsDataList.push(p);
        }
      });
    })
  }


  getPartners(typecode: string) {
    this.partnerList = [];
    this.restApiService.getList(this.newsURL,typecode, 36, 1).subscribe((dataList: any) => {
      dataList.forEach((p: any, index: number) => {
          this.partnerList.push(p);
      });
    })
  }

  public onClickDetail(id: any) {
    this.router.navigate([`/news/detail`, id]);
  }
}
