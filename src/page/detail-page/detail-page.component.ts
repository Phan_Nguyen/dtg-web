import {Component, OnInit} from '@angular/core';
import {ActivatedRoute, Router} from "@angular/router";
import {RestApiService} from "../../_services/rest-api.service";
import {URL_CONFIG} from "../../_config/url";

@Component({
  selector: 'app-detail-page',
  templateUrl: './detail-page.component.html',
  styleUrl: './detail-page.component.css'
})
export class DetailPageComponent implements OnInit {

  public routeID: any;
  public code: any;
  public url = URL_CONFIG.news;

  public dataDetail: any;
  public newsRelatedList: any[] = [];

  constructor(
    private restApiService: RestApiService,
    private activatedRoute: ActivatedRoute,
    private router: Router,
  ) {}

  ngOnInit() {
    this.activatedRoute.paramMap.subscribe((params: any) => {
      this.routeID = params.get('id');
      this.code = params.get('code');
      this.loadData();
    });
  }

  loadData() {
    if (!this.routeID) {
      return;
    }
    this.restApiService.multipleRequestInSameTime(
      this.restApiService.get(this.url + `/detail`, this.routeID),
    ).subscribe(([data]) => {
      this.dataDetail = data;
      this.restApiService.getList(this.url, data.Typeopt.Optiongroup.code, 5,1)
        .subscribe((dataList) => {
          this.newsRelatedList = dataList.filter((d: any) => d.id !== data.id);
        });
    })
  }

  onClickDetail(id: any) {
    if (this.code) {
      this.router.navigate([`/news/${this.code}/detail`, id]);
    } else {
      this.router.navigate([`/news/detail`, id]);
    }
  }

}
