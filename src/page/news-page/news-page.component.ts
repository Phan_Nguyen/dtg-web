import {Component, OnInit} from '@angular/core';
import {RestApiService} from "../../_services/rest-api.service";
import {ActivatedRoute, Router} from "@angular/router";
import {URL_CONFIG} from "../../_config/url";

@Component({
  selector: 'app-news-page',
  templateUrl: './news-page.component.html',
  styleUrls: ['./news-page.component.css']
})
export class NewsPageComponent implements OnInit {

  public page = 1;
  public pageSize = 9;
  public total = 0;
  public code = '';
  public newsURL = URL_CONFIG.news;
  public newsList: any[] = [];
  public isHideDetail = false;

  ngOnInit() {
    this.activatedRoute.paramMap.subscribe((q: any) => {
      this.code = q.get('code');
      this.getNews();
    });
  }

  constructor(
    private restApiService: RestApiService,
    private activatedRoute: ActivatedRoute,
    private router: Router,
    ) {}

  getNews() {
    this.restApiService.multipleRequestInSameTime(
      this.restApiService.getList(this.newsURL, this.code, this.pageSize, this.page),
      this.restApiService.getListSize(this.newsURL, this.code),
    ).subscribe(([newsList, count]) => {
      this.newsList = newsList;
      if(this.newsList.length > 0 && this.newsList[0].Typeopt?.Optiongroup?.config){
        const config = JSON.parse(this.newsList[0].Typeopt?.Optiongroup?.config);
        this.isHideDetail = config.isHideDetail? config.isHideDetail : false;

        console.log(config);
      }
      this.total= count;
    })
  }

  onChangePage(page: number) {
    this.page = page;
    this.pageSize = 9;
    this.getNews();
  }

  public onClickDetail(id: string) {
    if(!this.isHideDetail){
      this.router.navigate([`/news/${this.code}/detail`, id])

    }
  }
}
