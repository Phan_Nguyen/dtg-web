import {Component, Input, Output, EventEmitter} from '@angular/core';

@Component({
  selector: 'app-news-header',
  templateUrl: './news-header.component.html',
  styleUrl: './news-header.component.css'
})
export class NewsHeaderComponent {
  @Input() data: any;
  @Output() onClick = new EventEmitter();

  click() {
    this.onClick.emit();
  }

}
