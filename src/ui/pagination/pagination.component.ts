import {ChangeDetectionStrategy, Component, Input, Output, EventEmitter} from '@angular/core';

@Component({
  selector: 'app-pagination',
  templateUrl: './pagination.component.html',
  styleUrl: './pagination.component.css',
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class PaginationComponent {
  private _total: number = 0;
  private _page = 1;
  private _pageSize = 10;
  /**
   * Maximum of page to show in same time.
   */
  private _maxPage = 5;

  @Input('total') set total(_total: number) {
    this._total = _total;
  }

  @Input('page') set page(_page: number) {
    this._page = _page;
  }

  @Input('pageSize') set pageSize(_pageSize: number) {
    this._pageSize = _pageSize;
  }

  @Input('maxPage') set maxPage(_maxPage: number) {
    this._maxPage = _maxPage;
  }

  @Output() changePage = new EventEmitter<number>();

  get pager(): any {
    return this.paginate(this._total, this._page, this._pageSize, this._maxPage);
  }

  setPage(page: number) {
    if (this.pager.page === page || page > this.pager.totalPages || page === 0) {
      return;
    }

    this.changePage.emit(page);
  }

  private paginate(total: number, page: number = 1, pageSize: number = 10, maxPages: number = 7) {
    // calculate total pages
    const totalPages = Math.ceil(total / pageSize);

    // ensure current page isn't out of range
    if (page < 1) {
      page = 1;
    } else if (page > totalPages) {
      page = totalPages;
    }

    let startPage: number, endPage: number;
    if (totalPages <= maxPages) {
      // total pages less than max so show all pages
      startPage = 1;
      endPage = totalPages;
    } else {
      // total pages more than max so calculate start and end pages
      const maxPagesBeforeCurrentPage = Math.floor(maxPages / 2);
      const maxPagesAfterCurrentPage = Math.ceil(maxPages / 2) - 1;
      if (page <= maxPagesBeforeCurrentPage) {
        // current page near the start
        startPage = 1;
        endPage = maxPages;
      } else if (page + maxPagesAfterCurrentPage >= totalPages) {
        // current page near the end
        startPage = totalPages - maxPages + 1;
        endPage = totalPages;
      } else {
        // current page somewhere in the middle
        startPage = page - maxPagesBeforeCurrentPage;
        endPage = page + maxPagesAfterCurrentPage;
      }
    }

    // calculate start and end item indexes
    const startIndex = (page - 1) * pageSize;
    const endIndex = Math.min(startIndex + pageSize - 1, total - 1);

    // create an array of pages to ng-repeat in the pager control
    const pages = Array.from(Array(endPage + 1 - startPage).keys()).map((i) => startPage + i);

    // return object with all pager properties required by the view
    const returnValue = {
      total: total,
      page: page,
      pageSize: pageSize,
      totalPages: totalPages,
      startPage: startPage,
      endPage: endPage,
      startIndex: startIndex,
      endIndex: endIndex,
      pages: pages,
    };

    return returnValue;
  }
}
