import { Injectable } from '@angular/core';
import {HttpClient, HttpHeaders, HttpParameterCodec, HttpParams} from "@angular/common/http";
import {forkJoin, Observable} from "rxjs";


class CustomEncoder implements HttpParameterCodec {
  encodeKey(key: string): string {
    return encodeURIComponent(key);
  }

  encodeValue(value: string): string {
    return encodeURIComponent(value);
  }

  decodeKey(key: string): string {
    return decodeURIComponent(key);
  }

  decodeValue(value: string): string {
    return decodeURIComponent(value);
  }
}

@Injectable({
  providedIn: 'root'
})
export class RestApiService {

  constructor(private http: HttpClient) { }

  private static genHttpParams(paramObj: any): HttpParams {
    let _param = new HttpParams();
    for (const key in paramObj) {
      if (paramObj.hasOwnProperty(key) && paramObj[key] != null ) {
        if (typeof paramObj[key] === 'object') {
          _param = _param.set(key, JSON.stringify(paramObj[key]));
        } else {
          _param = _param.set(key, <string>paramObj[key]);
        }
      }
    }
    return _param;
  }

  /**
   * perform multiple request in a same time with rxjs forkJoin
   * @param tasks: array of http request
   */
  multipleRequestInSameTime(...tasks: Observable<any>[]) {
    return forkJoin(tasks);
  }

  /**
   * get list data
   * @param url url of request, ex: /api/tbl_customer,
   * @param typecode
   * @param param? param of request
   * @param notShowLoading?
   * @return Observable
   */
  getList(url: string, typecode: string,  pageSize?:number, page?:number, param?: object, notShowLoading?: boolean): Observable<any> {
    const paramObj = {
      pageSize: pageSize? pageSize : 20,
      page: page ? page : 1,
      ...param,
    };

    const _param = RestApiService.genHttpParams(paramObj);
    return this.http.get(`${url}/find/${typecode}`, {
      params: _param,
      headers: new HttpHeaders({
        notShowLoading: notShowLoading ? 'TRUE' : 'FALSE',
      }),
    });
  }
  getListSize<T>(url: string, param?: string, notShowLoading?: boolean): Observable<any> {
    return this.http.get<T[]>(`${url}/count/${param}`, {
      headers: new HttpHeaders({
        notShowLoading: notShowLoading ? 'TRUE' : 'FALSE',
      }),
    });
  }
  get<T>(url: string, id: any, notShowLoading?: boolean): Observable<any> {
    return this.http.get<T>(`${url}/${id}`, {
      headers: new HttpHeaders({
        notShowLoading: notShowLoading ? 'TRUE' : 'FALSE',
      }),
    });
    // .pipe(map((data) => new model(data)));
  }
}
