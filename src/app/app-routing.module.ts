import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import {HomePageComponent} from "../page/home-page/home-page.component";
import {NewsPageComponent} from "../page/news-page/news-page.component";
import {DetailPageComponent} from "../page/detail-page/detail-page.component";
import { ContactComponent } from '../page/contact/contact.component';



const routes: Routes = [
  {path: '',
    component: HomePageComponent,
  },
  {
    path: 'news/:code',
    component: NewsPageComponent
  },
  {
    path: 'news/:code',
    children: [
      {
        path: 'detail/:id',
        component: DetailPageComponent,
      },
    ]
  },
  {
    path: 'news/detail/:id',
    component: DetailPageComponent,
  },
  {
    path: 'contact',
    component: ContactComponent,
  },
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
