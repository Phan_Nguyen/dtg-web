import {Component, HostListener, OnInit} from '@angular/core';
import AOS from "aos";

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css'],
})
export class AppComponent implements OnInit {
  @HostListener("window:scroll", ["$event"])
  onWindowScroll() {
    const y = window.pageYOffset;
    const s = document.querySelector('#fixed-header');

    if (y > 0) {
      s? s.classList.add('fixed-top'): '';
    } else {
      s? s.classList.remove('fixed-top'): '';
    }
  }

  ngOnInit() {
    // init aos
    AOS.init();
    AOS.refresh();
    //
    }
}
