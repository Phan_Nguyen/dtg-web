import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { AppRoutingModule } from './app-routing.module';
import {AppComponent} from "./app.component";
import {BrowserModule} from "@angular/platform-browser";
import {HTTP_INTERCEPTORS, HttpClientModule} from "@angular/common/http";
import {HomePageComponent} from "../page/home-page/home-page.component";
import {FooterComponent} from "../_layout/footer/footer.component";
import {HeaderComponent} from "../_layout/header/header.component";
import {NewsPageComponent} from "../page/news-page/news-page.component";
import {NewsHeaderComponent} from "../page/news-page/news-header/news-header.component";
import {RouterModule} from "@angular/router";
import {PaginationComponent} from "../ui/pagination/pagination.component";
import {BreadcrumbComponent} from "../_layout/breadcrumb/breadcrumb.component";
import {DetailPageComponent} from "../page/detail-page/detail-page.component";
import { ContactComponent } from '../page/contact/contact.component';


@NgModule({
  declarations: [
    AppComponent,
    HomePageComponent,
    FooterComponent,
    HeaderComponent,
    NewsPageComponent,
    NewsHeaderComponent,
    PaginationComponent,
    BreadcrumbComponent,
    DetailPageComponent,
    ContactComponent
  ],
  imports: [
    CommonModule,
    AppRoutingModule,
    BrowserModule,
    HttpClientModule,
    RouterModule
  ],
  providers: [],
  bootstrap: [AppComponent],
})
export class AppModule { }
