if ($args.Count -eq 0)
{
  Write-Output "Please input version..."
  Write-Output "build.ps1 <version>"
  exit
}

Write-Output "Starting..."

$version = Write-Output $args[0]

Remove-Item -r -fo dist

Write-Output "npm install..."
Invoke-Expression "npm install"

Write-Output "npm build..."
Invoke-Expression "npm run build"

Write-Output "Building & publishing docker image..."
$( docker build . -t hub.bmes.vn/dtg-web:"$version" | Out-Host; $? ) -and $( docker push hub.bmes.vn/dtg-web:"$version" | Out-Host; $? )

Write-Output "Success!"
